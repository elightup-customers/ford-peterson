jQuery(function($){
	if ( $( 'body' ).hasClass( 'home' ) ) {
		var timer = null;
		var preventSlide = false;

		var wheel = Draggable.create("#wheel", {
			type:"rotation",
			throwProps:true,
			 snap:function(endValue) {
			  return Math.round(endValue / 90) * 90;
		   },
			onDrag: function() {
			  hideDetail();
			  pauseAutoplay();
			},
			onThrowComplete: function() {
			  dragActive()
			  resumeAutoplay();
			}
		});

		TweenMax.set('#wheel li:not(.active) .details > *', {
			opacity: 0,
			y: -10
		})

		// Calculate which product is active
		function dragActive() {
			var rot = wheel[0].rotation / 360
			var decimal = rot % 1
			var sliderLength = $('#wheel li').length
			var tempIndex = Math.round(sliderLength * decimal)
			var index

			if (rot < 0) {
				index = Math.abs(tempIndex)
			} else {
				index = sliderLength - tempIndex
		}

		if (decimal === 0) {
			index = 0
		}

		$('#wheel li.active').removeClass('active')
		$($('#wheel li')[index]).addClass('active')

		TweenMax.staggerTo('#wheel li.active .details > *', 0.6,   {
			opacity: 1,
			y: 0,
			delay: 0.5
		}, 0.1)

		}

		// Tween rotation
		function rotateDraggable(deg, callback) {
			var rot = wheel[0].rotation
			var tl = new TimelineMax()

			preventSlide = true

			tl
			.to('#wheel', .5, {
				rotation: rot + deg,
				delay: .3,
				onComplete: function() {
					preventSlide = false
					callback()
				}
			});

			wheel[0].rotation = rot + deg
		}

		// Handlers
		function nextHandler() {
			if (preventSlide) return

			var current = $('#wheel li.active')
			var item = current + 1
			if (item > $('#wheel li').length) {
				item = 1
			}
			rotateDraggable(360/$('#wheel li').length, dragActive);
			hideDetail();

			resumeAutoplay();
		}

		function prevHandler() {
			if (preventSlide) return

			var current = $('#wheel li.active')
			var item = current - 1
			if (item > 1) {
				item = $('#wheel li').length
			}

			rotateDraggable(-360/$('#wheel li').length, dragActive);
			hideDetail();
		}

		function handleNextButton(){
			nextHandler();

			resumeAutoplay();
		}

		function handlePrevButton(){
			prevHandler();

			resumeAutoplay();
		}

		function hideDetail(){
			TweenMax.to('#wheel li .details > *', 0.4,   {
				opacity: 0,
				y: -10
			})
		}

		function pauseAutoplay(){
			clearInterval(timer);
		}
		function resumeAutoplay(){
			clearInterval(timer);
			timer = setInterval(function(){
				nextHandler();
			}, 10000);
		}


		$('.next').on('click', handleNextButton);
		$('.prev').on('click', handlePrevButton);

		resumeAutoplay();

		$(window).focus(function() {
			resumeAutoplay();
		});

		$(window).blur(function() {
			pauseAutoplay();
		});
	}


	// Get the modal
	var modal = document.getElementById("myModal");

	// Get the button that opens the modal
	var btn = document.getElementById("myBtn");

	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[0];

	// When the user clicks the button, open the modal
	btn.onclick = function() {
		modal.style.display = "block";
	}

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
		modal.style.display = "none";
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
		if (event.target == modal) {
			modal.style.display = "none";
		}
	}


	$( '#login-form #user_login, #register-form #user_login' ).attr( 'placeholder', 'Username' );
	$( '#login-form #user_pass, #register-form #user_pass' ).attr( 'placeholder', 'Password' );
	$( '#register-form #user_email' ).attr( 'placeholder', 'Email' );
	$( '#register-form #user_pass2' ).attr( 'placeholder', 'Confirm password' );

	function popupForm() {
		$( '.popup-form' ).magnificPopup( {
			type: 'inline',
			preloader: false,
		} );
		$( '.btnbar' ).on( 'click', function() {
			$( 'body' ).addClass( 'footer-form' );
		} );
		$( '.close-btn' ).on( 'click', function() {
			$( 'body' ).removeClass( 'footer-form' );
		} );
	}

	function addPaddingLayout() {
		var primary   = $( '#primary' ).outerHeight(),
			form      = $( '#primary .entry-form' ).outerHeight(),
			secondary = $( '#secondary' ).outerHeight(),
			space     = ( primary - form ) / secondary;
		if ( space > 1.5 ) {
			var	padding_bottom = space*1000 + 'px';
		} else {
			padding_bottom = '500px';
		}

		if ( ( primary - form > secondary ) && ( $( window ).width() > 991 ) ) {
			$( '#secondary' ).css( 'padding-bottom', padding_bottom );
		}
		console.log( space,padding_bottom );
	}
	popupForm();
	addPaddingLayout();

});