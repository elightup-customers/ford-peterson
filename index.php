<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ford
 */

get_header();
?>

	<main id="primary" class="site-main">
		<p class="image-page__logo">
			<?php
			$blog_id = get_post_thumbnail_id( get_option('page_for_posts') );
			$img     = wp_get_attachment_image_src( $blog_id,'full');
			if ( ! empty( $blog_id ) ) {
				echo '<img src="' . $img[0] . '">';
			} else {
				?>
				<img src="<?php echo get_template_directory_uri() ?>/images/recent_icon_sm.png">
				<?php
			}
			?>
		</p>
		<div class="content-area">
			<?php
			if ( have_posts() ) :

				if ( is_home() && ! is_front_page() ) :
					?>
					<header class="entry-header">
						<h1 class="page-title"><?php single_post_title(); ?></h1>
						<img src="<?php echo get_template_directory_uri() ?>/images/heading_element.png">
					</header>
					<?php
				endif;

				/* Start the Loop */
				while ( have_posts() ) :
					the_post();

					/*
					 * Include the Post-Type-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
					 */
					get_template_part( 'template-parts/content', get_post_type() );

				endwhile;

				the_posts_navigation();
				ford_entry_form();

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif;
			?>
		</div>
	</main><!-- #main -->

<?php
get_sidebar();
get_footer();
