<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ford
 */

get_header();
?>

	<main id="primary" class="site-main">
		<p class="image-page__logo">
			<img src="<?php echo get_template_directory_uri() ?>/images/recent_icon_sm.png">
		</p>
		<div class="content-area">
			<?php if ( have_posts() ) : ?>

				<header class="entry-header">
					<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					?>
					<img src="<?php echo get_template_directory_uri() ?>/images/heading_element.png">
				</header><!-- .page-header -->

				<?php
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();

					/*
					 * Include the Post-Type-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
					 */
					get_template_part( 'template-parts/content', get_post_type() );

				endwhile;

				ford_entry_form();

				the_posts_navigation();

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif;
			?>
		</div>

	</main><!-- #main -->

<?php
get_sidebar();
get_footer();
