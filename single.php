<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ford
 */

get_header();
?>

	<main id="primary" class="site-main">
		<p class="image-page__logo">
			<img src="<?php echo get_template_directory_uri() ?>/images/recent_icon_sm.png">
		</p>
		<div class="content-area">
			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content', 'single' );

			endwhile; // End of the loop.
			?>
		</div>

	</main><!-- #main -->

<?php
get_sidebar();
get_footer();
