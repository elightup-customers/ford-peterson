<?php
/**
 * About widget.
 *
 * @package ford
 */

/**
 * About widget class.
 */
class Ford_Sidebar_Widget extends WP_Widget {
	/**
	 * Holds widget settings defaults, populated in constructor.
	 *
	 * @var array
	 */
	protected $defaults;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->defaults = array(
			'title'           => '',
			'image'           => '',
			'intro'           => '',
			'signature'       => '',
			'signature_image' => '',
		);

		parent::__construct(
			'ford-sidebar-widget',
			esc_html__( 'Ford: Sidebar Widget', 'ford' ),
			array(
				'classname' => 'ford-about-widget',
			)
		);

		add_action( 'sidebar_admin_setup', array( $this, 'enqueue_scripts' ) );
	}

	/**
	 * Enqueue script for image upload in widgets.
	 */
	public function enqueue_scripts() {
		wp_enqueue_style( 'ford-widget-image', get_template_directory_uri() . '/css/widget-image.css' );

		wp_enqueue_media();
		wp_enqueue_script( 'ford-widget-image', get_template_directory_uri() . '/js/widget-image.js', array(
			'jquery',
			'media-upload',
			'media-views',
		), '', true );
		wp_localize_script( 'eightydays-widget-image', 'EightyDaysWidgetImage', array(
			'title'  => esc_html__( 'Select an image', 'ford' ),
			'button' => esc_html__( 'Insert into widget', 'ford' ),
		) );
	}

	/**
	 * Display widget
	 *
	 * @param array $args Sidebar configuration.
	 * @param array $instance Widget settings.
	 */
	public function widget( $args, $instance ) {
		$instance = wp_parse_args( $instance, $this->defaults );

		echo $args['before_widget']; // WPCS: XSS OK.
		if ( $title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base ) ) {
			echo $args['before_title'] . esc_html( $title ) . $args['after_title'];  // WPCS: XSS OK.
		}
		if ( $instance['image'] ) {
			echo '<img class="ford-about-image" src="', esc_url( $instance['image'] ), '" alt="">';
		}
		if ( $instance['intro'] ) {
			echo '<div class="ford-about-intro">', wp_kses_post( $instance['intro'] ), '</div>';
		}

		if ( function_exists( 'jetpack_social_menu' ) ) {
			jetpack_social_menu();
		}

		if ( $instance['signature_image'] ) {
			echo '<img class="ford-about-signature" src="', esc_url( $instance['signature_image'] ), '" alt="">';
		}

		echo $args['after_widget']; // WPCS: XSS OK.
	}

	/**
	 * Update widget
	 *
	 * @param array $new_instance New widget settings.
	 * @param array $old_instance Old widget settings.
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		$instance                    = $old_instance;
		$instance['title']           = strip_tags( $new_instance['title'] );
		$instance['image']           = esc_url_raw( $new_instance['image'] );
		$instance['intro']           = wp_kses_post( $new_instance['intro'] );
		$instance['signature_image'] = esc_url_raw( $new_instance['signature_image'] );

		return $instance;
	}

	/**
	 * Display widget settings
	 *
	 * @param array $instance Widget settings.
	 *
	 * @return void
	 */
	public function form( $instance ) {
		$instance = wp_parse_args( $instance, $this->defaults );
		?>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'ford' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>"><?php esc_html_e( 'Image', 'ford' ); ?></label>
			<span class="eightydays-widget-image">
				<input id="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'image' ) ); ?>" type="text" class="eightydays-widget-image__input" value="<?php echo esc_attr( $instance['image'] ); ?>">
				<button class="button eightydays-widget-image__select"><?php esc_html_e( 'Select', 'ford' ); ?></button>
				<img src="<?php echo esc_url( $instance['image'] ); ?>" class="eightydays-widget-image__image<?php echo $instance['image'] ? '' : ' hidden'; ?>">
			</span>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'intro' ) ); ?>"><?php esc_html_e( 'Intro', 'ford' ); ?></label>
			<textarea id="<?php echo esc_attr( $this->get_field_id( 'intro' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'intro' ) ); ?>" class="widefat"><?php echo esc_textarea( $instance['intro'] ); ?></textarea>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'signature_image' ) ); ?>"><?php esc_html_e( 'Signature Image', 'ford' ); ?></label>
			<span class="eightydays-widget-image">
				<input id="<?php echo esc_attr( $this->get_field_id( 'signature_image' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'signature_image' ) ); ?>" type="text" class="eightydays-widget-image__input" value="<?php echo esc_attr( $instance['signature_image'] ); ?>">
				<button class="button eightydays-widget-image__select"><?php esc_html_e( 'Select', 'ford' ); ?></button>
				<img src="<?php echo esc_url( $instance['signature_image'] ); ?>" class="eightydays-widget-image__image<?php echo $instance['signature_image'] ? '' : ' hidden'; ?>">
			</span>
			<span class="description"><?php esc_html_e( 'This option overrides signature text above.', 'ford' ); ?></span>
		</p>
		<?php
	}
}
