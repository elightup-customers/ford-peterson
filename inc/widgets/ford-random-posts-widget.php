<?php
/**
 * Random posts widget
 * Get random posts and display in widget
 *
 * @package ford
 */

/**
 * Random posts class.
 */
class Ford_Random_Posts_Widget extends WP_Widget {
	/**
	 * Default widget options.
	 *
	 * @var array
	 */
	protected $defaults;

	/**
	 * Widget setup.
	 */
	public function __construct() {
		$this->defaults = array(
			'title'     => esc_html__( 'Random Posts', 'ford' ),
			'number'    => 3,
			'show_date' => true,
			'image'     =>  '',
		);
		parent::__construct(
			'ford-random-posts',
			esc_html__( 'Ford: Random Posts', 'ford' ),
			array(
				'classname' => 'ford-random-posts ford-sidebar',
				'description' => esc_html__( 'A widget that displays your random posts', 'ford' ),
			)
		);

		add_action( 'sidebar_admin_setup', array( $this, 'enqueue_scripts' ) );
	}

	/**
	 * Enqueue script for image upload in widgets.
	 */
	public function enqueue_scripts() {
		wp_enqueue_style( 'ford-widget-image', get_template_directory_uri() . '/css/widget-image.css' );

		wp_enqueue_media();
		wp_enqueue_script( 'ford-widget-image', get_template_directory_uri() . '/js/widget-image.js', array(
			'jquery',
			'media-upload',
			'media-views',
		), '', true );
		wp_localize_script( 'ford-widget-image', 'FordWidgetImage', array(
			'title'  => esc_html__( 'Select an image', 'ford' ),
			'button' => esc_html__( 'Insert into widget', 'ford' ),
		) );
	}

	/**
	 * How to display the widget on the screen.
	 *
	 * @param array $args     Widget parameters.
	 * @param array $instance Widget instance.
	 */
	public function widget( $args, $instance ) {
		$instance = wp_parse_args( $instance, $this->defaults );
		$query    = new WP_Query( array(
			'posts_per_page' => absint( $instance['number'] ),
			'orderby'        => 'rand',
		) );
		if ( ! $query->have_posts() ) {
			return;
		}

		echo $args['before_widget']; // WPCS: XSS OK.
		?>
		<p class="ford-sidebar__icon">
			<?php
			if ( $instance['image'] ) {
				echo '<img class="ford-about-image" src="', esc_url( $instance['image'] ), '" alt="">';
			}
			?>
			<!-- <img src="<?php echo get_template_directory_uri() ?>/images/recent_icon_sm.png"> -->
		</p>
		<div class="widget-content ford-sidebar__text">
			<?php
			$title = apply_filters( 'widget_title', $instance['title'] );
			if ( $title ) {
				echo $args['before_title'], $title , $args['after_title']; // WPCS: XSS OK.
			}

			$i = 1;
			while ( $query->have_posts() ) : $query->the_post(); ?>
			<?php
			if ( $i > absint( $instance['number'] ) ) {
				break;
			} ?>
				<article class="aside-post ford-sidebar__item">
					<div class="ford-sidebar__content">
						<h4 class="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
						<?php the_excerpt(); ?>
						<div class="ford-sidebar__meta">
							<span class="posted-on">
								<?php echo esc_html( get_the_date() ); ?>
							</span>
							<span class="read-more">
								<a href="<?php the_permalink(); ?>"><?php echo esc_html__( 'Read more', 'ford' ); ?></a>
							</span>
						</div>
					</div>
					<div class="ford-sidebar__img">
						<a class="image" href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
					</div>
				</article>
			<?php $i++;
			endwhile; ?>
			<div class="ford-sidebar__readmore">
				<a href="<?php echo home_url(); ?>/blog"><?php echo esc_html__( 'Read more', 'ford' ); ?></a>
			</div>
		</div>
		<?php
		wp_reset_postdata();

		echo $args['after_widget']; // WPCS: XSS OK.
	}

	/**
	 * Update the widget settings.
	 *
	 * @param array $new_instance New widget instance.
	 * @param array $old_instance Old widget instance.
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title']  = sanitize_text_field( $new_instance['title'] );
		$instance['image']  = $new_instance['image'];
		$instance['number'] = absint( $new_instance['number'] );

		return $instance;
	}

	/**
	 * Widget form.
	 *
	 * @param array $instance Widget instance.
	 *
	 * @return void
	 */
	public function form( $instance ) {
		$instance  = wp_parse_args( $instance, $this->defaults ); ?>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'ford' ); ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>"><?php esc_html_e( 'Icon', 'ford' ); ?></label>
			<span class="ford-widget-image">
				<input id="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'image' ) ); ?>" type="text" class="ford-widget-image__input" value="<?php echo esc_attr( $instance['image'] ); ?>">
				<button class="button ford-widget-image__select"><?php esc_html_e( 'Select', 'ford' ); ?></button>
				<img src="<?php echo esc_url( $instance['image'] ); ?>" class="ford-widget-image__image<?php echo $instance['image'] ? '' : ' hidden'; ?>">
			</span>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Number of posts to show:', 'ford' ); ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" value="<?php echo absint( $instance['number'] ); ?>" size="3">
		</p>
		<?php
	}
}
