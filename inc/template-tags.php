<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package ford
 */

if ( ! function_exists( 'ford_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time.
	 */
	function ford_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf(
			$time_string,
			esc_attr( get_the_date( DATE_W3C ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( DATE_W3C ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			/* translators: %s: post date. */
			esc_html_x( 'Posted on %s', 'post date', 'ford' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

		echo '<span class="posted-on">' . $posted_on . '</span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

	}
endif;

if ( ! function_exists( 'ford_posted_by' ) ) :
	/**
	 * Prints HTML with meta information for the current author.
	 */
	function ford_posted_by() {
		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( 'by %s', 'post author', 'ford' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo '<span class="byline"> ' . $byline . '</span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

	}
endif;

if ( ! function_exists( 'ford_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function ford_entry_footer() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( esc_html__( ', ', 'ford' ) );
			if ( $categories_list ) {
				/* translators: 1: list of categories. */
				printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'ford' ) . '</span>', $categories_list ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			}

			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'ford' ) );
			if ( $tags_list ) {
				/* translators: 1: list of tags. */
				printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'ford' ) . '</span>', $tags_list ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			}
		}

		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">';
			comments_popup_link(
				sprintf(
					wp_kses(
						/* translators: %s: post title */
						__( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'ford' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					wp_kses_post( get_the_title() )
				)
			);
			echo '</span>';
		}

		edit_post_link(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Edit <span class="screen-reader-text">%s</span>', 'ford' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				wp_kses_post( get_the_title() )
			),
			'<span class="edit-link">',
			'</span>'
		);
	}
endif;

if ( ! function_exists( 'ford_post_thumbnail' ) ) :
	/**
	 * Displays an optional post thumbnail.
	 *
	 * Wraps the post thumbnail in an anchor element on index views, or a div
	 * element when on single views.
	 */
	function ford_post_thumbnail() {
		if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
			return;
		}

		if ( is_singular() ) :
			?>

			<div class="post-thumbnail">
				<?php the_post_thumbnail(); ?>
			</div><!-- .post-thumbnail -->

		<?php else : ?>

			<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
				<?php
					the_post_thumbnail(
						'post-thumbnail',
						array(
							'alt' => the_title_attribute(
								array(
									'echo' => false,
								)
							),
						)
					);
				?>
			</a>

			<?php
		endif; // End is_singular().
	}
endif;

if ( ! function_exists( 'wp_body_open' ) ) :
	/**
	 * Shim for sites older than 5.2.
	 *
	 * @link https://core.trac.wordpress.org/ticket/12563
	 */
	function wp_body_open() {
		do_action( 'wp_body_open' );
	}
endif;

/**
 * Filter the except length to 20 words.
 *
 * @return int Modified excerpt length.
 */
function ford_custom_excerpt_length() {
	return 12;
}
add_filter( 'excerpt_length', 'ford_custom_excerpt_length' );

/**
 * Change the [...] to a 'continue reading' link automatically.
 *
 * @return string.
 */
function ford_excerpt_more() {
	return '';
}
add_action( 'excerpt_more', 'ford_excerpt_more' );

// Entry form.
function ford_entry_form() {
	?>
	<div id="entry-form" class="entry-form">
		<?php if ( is_page( 'candidate' ) || is_page( 'employer' ) ) : ?>
		<p class="entry-form__icon">
			<?php
			$icon_forms = rwmb_meta( 'icon_form', ['size' => 'full'], $page_id );
			$icon_form  = reset( $icon_forms );
			if ( $icon_form ) : ?>

				<img src="<?php echo $icon_form['url']; ?>">
			<?php else : ?>

				<img src="<?php echo get_template_directory_uri() ?>/images/candidate_res_icon_med_001.png">

			<?php endif; ?>
		</p>
		<?php endif; ?>
		<div class="entry-form__header">
			<img src="<?php echo get_template_directory_uri() ?>/images/ford_peterson_logo_home_white.png">
			<span>
			<?php
			$title_form = rwmb_meta( 'title_form' );
			if ( empty( $title_form ) ) {
				if ( ! is_user_logged_in() ) {
					$title_form = 'Subscribe';
				} else {
					$title_form = 'Contact';
				}
			}
			echo $title_form;
			?>
			</span>
		</div>
		<?php if ( ! is_user_logged_in() ) : ?>
			<div class="entry-form__body">
				<?php
				$form_login    = '[mb_user_profile_login label_submit="Log in" label_remember="Remember your login" label_lost_password="Lost password?"]';
				$form_register = '[mb_user_profile_register id="form" label_submit="Register" confirmation="Your account has been created successfully!" password_strength=false]';
				echo '<h3>' . esc_html__( 'Login', 'ford' ) . '</h3>';
				echo do_shortcode( $form_login );
				echo '<h3>' . esc_html__( 'Sign up', 'ford' ) . '</h3>';
				echo do_shortcode( $form_register );
				?>
			</div>
			<div class="entry-form__bluebar">

			</div>
		<?php endif; ?>

		<?php if ( is_user_logged_in() ) :
			$user       = wp_get_current_user();
			$roles_user = $user->roles[0];
			?>

			<?php if ( is_page( 'candidate' ) && ( $roles_user == 'candidate' || $roles_user = 'administrator' ) ) : ?>
				<div class="entry-access-button">
					<a href="<?php echo esc_url( home_url() ); ?>/candidate-resource-2/">Access careers resource centre</a>
				</div>
			<?php elseif ( ( is_page( 'candidate' ) || is_page( 'candidate-resource-2' ) ) && ( $roles_user == 'employer' ) ) : ?>
				<div style="text-align: center; padding: 20px;">Logged in as employer. Logout and log in as jobseeker to acces this content</div>
				<div class="entry-access-button">
					<a href="<?php echo esc_url ( wp_logout_url( get_permalink() ) ); ?>">Log out</a>
				</div>
			<?php elseif ( is_page( 'employer' ) && ( $roles_user == 'employer' || $roles_user = 'administrator' ) ) : ?>
				<div class="entry-access-button">
					<a href="<?php echo esc_url( home_url() ); ?>/employer-resource/">Access employer resource centre</a>
				</div>
			<?php elseif ( ( is_page( 'employer' ) || is_page( 'employer-resource' ) ) && ( $roles_user == 'candidate' ) ) : ?>
				<div style="text-align: center; padding: 20px;">Logged in as jobseeker. Logout and log in as employer to acces this content</div>
				<div class="entry-access-button">
					<a href="<?php echo esc_url ( wp_logout_url( get_permalink() ) ); ?>">Log out</a>
				</div>
			<?php else : ?>
				<div class="entry-form__footer">
					<?php
					if ( is_page( 'candidate-resource-2' ) && ( $roles_user == 'candidate' || $roles_user = 'administrator' ) ) {
						echo '<h3>' . esc_html__( 'CV Document', 'ford' ) . '</h3>';
						echo do_shortcode( '[gravityform id="2" title="false" description="false" ajax="true"]' );
					} else {
						echo '<h3>' . esc_html__( 'Message', 'ford' ) . '</h3>';
						echo do_shortcode( '[gravityform id="1" title="false" description="false" ajax="true"]' );
					}
					?>
				</div>
			<?php endif; ?>
		<?php endif; ?>
	</div>
	<?php
}
