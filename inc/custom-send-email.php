<?php
function ford_send_welcome_email( $config, $user_id ) {
	$user       = get_userdata( $user_id );
	$user_meta  = get_user_meta( $user_id );
	$user_email = $user->user_email;
	// for simplicity, lets assume that user has typed their first and last name when they sign up
	$user_full_name = $user_meta['first_name'][0] . ' ' . $user_meta['last_name'][0];

	// Now we are ready to build our welcome email
	$to = $user_email;
	$subject = "Hi " . $user_full_name . ", welcome to our site!";
	$body = '
			<h1>Dear ' . $user_full_name . ',</h1></br>
			<p>Thank you for joining our site. Your account is now active.</p>
			<p>Please go ahead and navigate around your account.</p>
			<p>Let me know if you have further questions, I am here to help.</p>
			<p>Enjoy the rest of your day!</p>
			<p>Kind Regards,</p>
	';
	$roles = $user_meta['radio_choice_roles'][0];
	$body_email_admin = '
			<h1>A new user has been just submitted.</h1></br>
			<p><b>Here is detail:</b></p>
			<p>Name: ' . $user_full_name . '</p>
			<p>User Email: ' . $user_email . ' </p>
			<p>Roles: ' . $roles . '</p>
	';
	$headers = array( 'Content-Type: text/html; charset=UTF-8' );
	if ( 'form' === $config['id'] ) {
		wp_mail( $to, $subject, $body, $headers );
		wp_mail( 'careers@fphq.com.au', 'New submission', $body_email_admin, $headers );
	}
}
add_action( 'rwmb_profile_after_process', 'ford_send_welcome_email', 10, 2 );
