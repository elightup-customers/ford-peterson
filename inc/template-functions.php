<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package ford
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function ford_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	$user       = wp_get_current_user();
	$roles_user = $user->roles[0];
	if ( $roles_user == 'candidate' ) {
		$classes[] = 'is-candidate';
	}

	if ( $roles_user == 'employer' ) {
		$classes[] = 'is-employer';
	}

	if ( $roles_user == 'administrator' ) {
		$classes[] = 'is-admin';
	}

	return $classes;
}
add_filter( 'body_class', 'ford_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function ford_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'ford_pingback_header' );
