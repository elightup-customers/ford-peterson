<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ford
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<div id="page" class="site">
	<?php
	// Check category_page or blog page.
	if ( is_404() || is_single() || is_category() || ( is_home() && ! is_front_page() ) ) {
		$style = '';
	} else {
		$banners = rwmb_meta( 'image_banner', ['size' => 'full'], get_the_ID() );
		$banner  = reset( $banners );
		$style   = $banner ? 'style="background: url( ' . $banner['url'] . ' )"' : '';
	}
	?>
	<header class="main_header" <?php echo wp_kses_post( $style ); ?>>
		<!-- logo -->
		<div class="logo">
			<a href="<?php echo esc_url( home_url() ); ?>"><img class="logotype" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/ford_peterson_logo_home.png" alt="Ford Peterson logo"></a>
		</div>
		<!-- circular navigation -->
		<section class="menu menu--circle">
			<input type="checkbox" id="menu__active"/>
			<label for="menu__active" class="menu__active">
				<div class="menu__toggle">
				  <div class="icon-nav">
					<div class="hamburger-nav"></div>
				  </div>
				</div>
				<input name="arrow--up" type="radio" id="degree--up-0" value="hidden"/>
				<input type="radio" name="arrow--up" id="degree--up-1" />
				<input type="radio" name="arrow--up" id="degree--up-2" />
				<?php
				$ford_walker = new Ford_Walker;
				wp_nav_menu( array(
					'container_class' => 'menu__listings',
					'menu_class'      => 'circle',
					'theme_location'  => 'menu-1',
					'walker'          => $ford_walker,
				) );
				?>
				<div class="menu__arrow menu__arrow--top">
				  <ul>
					<li>
					  <label for="degree--up-0">
					  <div class="arrow"></div>
					  </label>
					  <label for="degree--up-1">
					  <div class="arrow"></div>
					  </label>
					  <label for="degree--up-2">
					  <div class="arrow"></div>
					  </label>
					</li>
				  </ul>
				</div>
			</label>
		</section>

	</header>
	<div class="nav-bar">
		<div class="nav-bar--left"><a href="#form-login" class="popup-form">Login</a> | <a href="#form-signup" class="popup-form">Sign up</a></div>
		<div class="nav-bar--right"><a href="tel:0280073632">Sydney (02) 8007 3632</a> | <a href="tel:0390284862">Melbourne (03) 9028 4862</a></div>
	</div>
	<div id="form-login" class="entry-form mfp-hide white-popup-block">
		<div class="entry-form__header">
			<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/ford_peterson_logo_home_white.png">
			<span>Subscribe</span>
		</div>
		<div class="entry-form__body">
			<?php
			$form_login    = '[mb_user_profile_login label_submit="Log in" label_remember="Remember your login" label_lost_password="Lost password?"]';
			echo '<h3>' . esc_html__( 'Login', 'ford' ) . '</h3>';
			echo do_shortcode( $form_login );
			?>
		</div>
		<div class="entry-form__bluebar"></div>
	</div>
	<div id="form-signup" class="entry-form mfp-hide white-popup-block">
		<div class="entry-form__header">
			<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/ford_peterson_logo_home_white.png">
			<span>Subscribe</span>
		</div>
		<div class="entry-form__body">
			<?php
			$form_register = '[mb_user_profile_register id="form" label_submit="Register" confirmation="Your account has been created successfully!" password_strength=false]';
			echo '<h3>' . esc_html__( 'Sign up', 'ford' ) . '</h3>';
			echo do_shortcode( $form_register );
			?>
		</div>
		<div class="entry-form__bluebar"></div>
	</div>
	<div id="content" class="site-content">
