<?php
/**
 * ford functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ford
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'ford_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function ford_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on ford, use a find and replace
		 * to change 'ford' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'ford', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'ford' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'ford_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);

		/**
		 * Add support for the block editor.
		 *
		 * @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/
		 */
		add_theme_support( 'wp-block-styles' );
		add_theme_support( 'align-wide' );
		add_theme_support( 'editor-styles' );
		add_theme_support( 'responsive-embeds' );
	}
endif;
add_action( 'after_setup_theme', 'ford_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ford_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'ford_content_width', 640 );
}
add_action( 'after_setup_theme', 'ford_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ford_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'ford' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'ford' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_widget( 'Ford_Sidebar_Widget' );
	register_widget( 'Ford_Recent_Posts_Widget' );
	register_widget( 'Ford_Widget_Categories' );
	register_widget( 'Ford_Widget_Archives' );
	register_widget( 'Ford_Random_Posts_Widget' );
}
add_action( 'widgets_init', 'ford_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function ford_scripts() {
	wp_enqueue_style( 'font-awesome', 'https://cdn.jsdelivr.net/fontawesome/4.7.0/css/font-awesome.min.css', '', '4.7.0' );
	wp_enqueue_style( 'ford-magnific-popup', get_template_directory_uri() . '/css/magnific-popup.css', '', '' );
	wp_enqueue_style( 'ford-style', get_stylesheet_uri(), '', '1.0.1' );
	wp_style_add_data( 'ford-style', 'rtl', 'replace' );

	wp_enqueue_script( 'ford-magnific', get_template_directory_uri() . '/js/jquery.magnific-popup.js', array(), '20151215', true );
	wp_enqueue_script( 'ford-tweenmax', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TweenMax.min.js' );
	wp_enqueue_script( 'ford-draggable', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/utils/Draggable.min.js' );
	wp_enqueue_script( 'ford-cssplugin', 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/152635/CSSPlugin.js' );
	wp_enqueue_script( 'ford-throwpropsplugin', 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/152635/ThrowPropsPlugin.min.js' );
	wp_enqueue_script( 'ford-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'ford-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'ford', get_template_directory_uri() . '/js/script.js', array( 'jquery' ), '1.0', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'ford_scripts' );

/**
 * Add editor style.
 */
function ford_add_editor_styles() {
	add_editor_style( 'style-editor.css' );
}
add_action( 'init', 'ford_add_editor_styles' );

/**
 * Add admin style.
 */
function for_admin_style() {
	wp_enqueue_style( 'ford-admin-style', get_template_directory_uri() . '/css/admin.css', '', '' );
}
add_action( 'admin_enqueue_scripts', 'for_admin_style' );


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Include widget file
 */
require get_template_directory() . '/inc/widgets/ford-sidebar-widget.php';
require get_template_directory() . '/inc/widgets/ford-recent-posts-widget.php';
require get_template_directory() . '/inc/widgets/ford-random-posts-widget.php';
require get_template_directory() . '/inc/widgets/ford-widget-categories.php';
require get_template_directory() . '/inc/widgets/ford-widget-archives.php';

/**
 * Include widget file
 */
require get_template_directory() . '/inc/ford-custom-menu.php';

/**
 * Send email when new user register.
 */
require get_template_directory() . '/inc/custom-send-email.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Change role after register user.
 */
function ford_change_role( $object ) {
	$meta_user = get_user_meta( $object->user_id );
	$roles     = $meta_user['radio_choice_roles'];
	$user      = new WP_User( $object->user_id );

	if ( $roles[0] == 'job_seeker' ) {
		$user->set_role( 'candidate' );
	}
	if ( $roles[0] == 'employer' ) {
		$user->set_role( 'employer' );
	}

	// Login after register.
	wp_set_current_user( $object->user_id, $meta_user['nickname'] );
	wp_set_auth_cookie( $object->user_id );
	do_action( 'wp_login', $meta_user['nickname'], $user );
}
add_action( 'rwmb_profile_after_save_user', 'ford_change_role' );

/**
 * Redirect after login.
 */
function ford_redirect_after_login() {
	if ( 'success' !== filter_input( INPUT_GET, 'rwmb-form-submitted' ) ) {
		return;
	}
	$user       = wp_get_current_user();
	$roles_user = $user->roles[0];
	$redirect   = '';
	if ( $roles_user == 'candidate' ) {
		$redirect = home_url() . '/candidate/';
	} elseif ( $roles_user == 'employer' ) {
		$redirect = home_url() . '/employer/';
	} else {
		$redirect = home_url();
	}
	if ( $redirect && is_front_page() ) {
		wp_safe_redirect( $redirect );
	}
}
add_filter( 'template_redirect', 'ford_redirect_after_login' );

/**
 * Insert image after title.
 */
function ford_insert_image_after_title( $title, $id = null ) {
	if ( ! is_admin() ) {
		$title = $title . '<img src="' . get_template_directory_uri() . '/images/heading_element.png"/>';
	}
	return $title;
}
add_filter( 'the_title', 'ford_insert_image_after_title', 10, 2 );

/**
 * Change login logo.
 */
function ford_change_login_logo() {
	?>
	<style type="text/css">
		#login h1 a, .login h1 a {
			background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/ford_peterson_logo_home.png);
			height:65px;
			width:320px;
			background-size: 200px 74px;
			background-repeat: no-repeat;
			padding-bottom: 30px;
		}
		.login #login #backtoblog {
			font-size: 16px;
		}
	</style>
	<?php
}
add_action( 'login_enqueue_scripts', 'ford_change_login_logo' );

/**
 * Redirect to current page when logout.
 */
function ford_logout_redirect( $logouturl, $redir ) {
	if ( is_page( 'employer' ) || is_page( 'employer-resource' ) || is_page( 'candidate' ) || is_page( 'candidate-resource-2' ) ) {
		return $logouturl;
	} else {
		return $logouturl . '&amp;redirect_to=http://' . $_SERVER['HTTP_HOST'] . strtok( $_SERVER['REQUEST_URI'], '?' );
	}

}
add_filter( 'logout_url', 'ford_logout_redirect', 10, 2 );

/**
 * Change login logo link.
 */
function ford_change_link_login_logo() {
	return home_url();
}
add_action( 'login_headerurl', 'ford_change_link_login_logo' );

/**
 * Shortcode submit CV widget.
 */
function ford_shortcode_submit_CV() {
	ob_start();
	?>
	<div class="ford-sidebar">
		<p class="ford-sidebar__icon"><img class="logo-title alignnone size-full wp-image-10" src="<?php echo get_template_directory_uri() ?>/images/submit_cv_icon_sm.png" alt="" width="102" height="96" /></p>
		<div class="ford-sidebar__text">
			<div class="ford-sidebar__item">
				<div class="ford-sidebar__content">
					<h4>Submit Your CV</h4>
					<p>Send your CV to us now, we look forward to it.<br><br>
					If you are having trouble putting your CV together, why don't you try our user friendly CV template.
					</p>
				</div>
				<div class="ford-sidebar__img">
					<a href="#"><img class="size-full wp-image-13 alignnone" src="<?php echo get_template_directory_uri() ?>/images/upload_icon_001.png" alt="" width="101" height="101" /></a>
				</div>
			</div>
			<?php
			$user       = wp_get_current_user();
			$roles_user = $user->roles[0];
			if ( is_user_logged_in() && ( $roles_user == 'candidate' || $roles_user == 'administrator' ) ) :
			?>
				<div class="ford-sidebar__submit">
					<a class="btn-main" href="<?php echo esc_url( home_url() ); ?>/candidate-resource-2/#entry-form">Submit</a>
				</div>
			<?php else : ?>
				<div class="ford-sidebar__login" style="margin-top: 30px;">
					<a class="btn-main popup-form" href="#form-signup">sign up</a>
					<a class="btn-main popup-form" href="#form-login">log in</a>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<?php
	return ob_get_clean();
}
add_shortcode( 'shortcode_submit_CV', 'ford_shortcode_submit_CV' );


/**
 * Shortcode submit CV widget.
 */
function ford_shortcode_submit_job() {
	ob_start();
	?>
	<div class="ford-sidebar">
		<p class="ford-sidebar__icon"><img class="logo-title alignnone size-full wp-image-10" src="<?php echo get_template_directory_uri() ?>/images/employer_res_icon_sm_002.png" alt="" width="102" height="96" /></p>
		<div class="ford-sidebar__text">
			<div class="ford-sidebar__item">
				<div class="ford-sidebar__content">
					<h4>Submit Job Post</h4>
					<p>We're in regular contact with hundreds of accountants right across Australia. We have an extensive database.<br><br>
					Simply provide us with the information we need to fill a role and we'll provide you with the best person for the job.
					</p>
				</div>
				<div class="ford-sidebar__img">
					<a href="#"><img class="size-full wp-image-13 alignnone" src="<?php echo get_template_directory_uri() ?>/images/upload_icon_001.png" alt="" width="101" height="101" /></a>
				</div>
			</div>
			<?php
			$user       = wp_get_current_user();
			$roles_user = $user->roles[0];
			if ( is_user_logged_in() && ( $roles_user == 'employer' || $roles_user == 'administrator' ) ) :
			?>
			<div class="ford-sidebar__submit">
				<a class="btn-main" href="<?php echo esc_url( home_url() ); ?>/employer-resource/#entry-form">Submit</a>
			</div>
			<?php else : ?>
				<div class="ford-sidebar__login" style="margin-top: 30px;">
					<a class="btn-main popup-form" href="#form-signup">sign up</a>
					<a class="btn-main popup-form" href="#form-login">log in</a>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<?php
	return ob_get_clean();
}
add_shortcode( 'shortcode_submit_job', 'ford_shortcode_submit_job' );