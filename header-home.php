<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ford
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body ontouchstart <?php body_class(); ?>>
<?php wp_body_open(); ?>
	<div class="home-wrapper">
		<!-- arc-slider -->
		<svg class="svg-mask" width="0" height="0">
			<defs>
				<clippath id="quarterMask" clipPathUnits="objectBoundingBox">
					<path d="M.731.26556a.32673.32673,0,0,1-.46192,0L.00512.52947a.6999.6999,0,0,0,.9898,0Z"/>
				</clippath>
			</defs>
		</svg>
		<div class="content">
			<?php
			if ( is_user_logged_in() ) {
				$user       = wp_get_current_user();
				$roles_user = $user->roles[0];
			}
			?>
			<div id="wheel-border"></div>
			<ul class="featured-slider" id="wheel">
				<li class="active">
					<div class="details">
						<h1 class="title"><i title="candidates" class="fa fa-user-circle-o"></i> Job Seekers</h1>
						<?php if ( is_user_logged_in() && ( $roles_user == 'candidate' || $roles_user == 'administrator' ) ) : ?>
							<a href="<?php echo esc_url( home_url() ); ?>/candidate" target="_blank" class="button">find out more</a>
						<?php else : ?>
							<a href="#entry-form__signup" target="_blank" class="button popup-form">sign up</a>
							<a href="#entry-form__login" target="_blank" class="button popup-form">log in</a>
						<?php endif; ?>
					</div>
					<div class="image"> <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/image_001.jpg" alt="" /> </div>
				</li>
				<li>
					<div class="details">
						<h1 class="title"><i title="candidate resource" class="fa fa-cog"></i> Resources</h1>
						<?php if ( ! is_user_logged_in() ) : ?>
							<a href="#entry-form__signup" target="_blank" class="button popup-form">sign up</a>
							<a href="#entry-form__login" target="_blank" class="button popup-form">log in</a>
						<?php else : ?>
							<a href="<?php echo esc_url( home_url() ); ?>/candidate-resource-2/" target="_blank" class="button">job seekers</a>
							<a href="<?php echo esc_url( home_url() ); ?>/employer-resource" target="_blank" class="button">employers</a>
						<?php endif; ?>
					</div>
					<div class="image"> <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/image_003.jpg" alt="" /> </div>
				</li>
				<li>
					<div class="details">
						<h1 class="title"><i title="events" class="fa fa-calendar"></i> Events</h1>
						<?php if ( ! is_user_logged_in() ) : ?>
							<a href="#entry-form__signup" target="_blank" class="button popup-form">sign up</a>
							<a href="#entry-form__login" target="_blank" class="button popup-form">log in</a>
						<?php else : ?>
							<a href="<?php echo esc_url( home_url() ); ?>/reward/" target="_blank" class="button">find out more</a>
						<?php endif; ?>
					</div>
					<div class="image"> <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/image_004.jpg" alt="" /> </div>
				</li>
				<li>
					<div class="details">
						<h1 class="title"><i title="employers" class="fa fa-briefcase"></i> Employers</h1>
						<?php if ( is_user_logged_in() && ( $roles_user == 'employer' || $roles_user == 'administrator' ) ) : ?>
							<a href="<?php echo esc_url( home_url() ); ?>/employer/" target="_blank" class="button">find out more</a>
						<?php else : ?>
							<a href="#entry-form__signup" target="_blank" class="button popup-form">sign up</a>
							<a href="#entry-form__login" target="_blank" class="button popup-form">log in</a>
						<?php endif; ?>
					</div>
					<div class="image"> <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/image_002.jpg" alt="" /> </div>
				</li>
			</ul>
			<nav>
				<a class="prev"><i class="fa fa-chevron-left"></i></a>
				<a class="next"><i class="fa fa-chevron-right"></i></a>
			</nav>

			<div id="entry-form__login" class="entry-form mfp-hide white-popup-block">
				<div class="entry-form__header">
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/ford_peterson_logo_home_white.png">
					<span>Subscribe</span>
				</div>
				<div class="entry-form__body">
					<?php
					$form_login = '[mb_user_profile_login label_submit="Log in" label_remember="Remember your login" label_lost_password="Lost password?"]';
					echo '<h3>' . esc_html__( 'Login', 'ford' ) . '</h3>';
					echo do_shortcode( $form_login );
					?>
				</div>
				<div class="entry-form__bluebar"></div>
			</div>
			<div id="entry-form__signup" class="entry-form mfp-hide white-popup-block">
				<div class="entry-form__header">
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/ford_peterson_logo_home_white.png">
					<span>Subscribe</span>
				</div>
				<div class="entry-form__body">
					<?php
					$form_register = '[mb_user_profile_register id="form" label_submit="Register" confirmation="Your account has been created successfully!" password_strength=false]';
					echo '<h3>' . esc_html__( 'Sign up', 'ford' ) . '</h3>';
					echo do_shortcode( $form_register );
					?>
				</div>
				<div class="entry-form__bluebar"></div>
			</div>
		</div>
		<!-- logo -->
		<div class="logo">
			<a href="<?php echo esc_url( home_url() ); ?>"><img class="logotype" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/ford_peterson_logo_home.png" alt="Ford Peterson logo"></a>
			<h4><a href="tel:0280073632">Sydney (02) 8007 3632</a></h4>
			<hr>
			<h4><a href="tel:0390284862">Melbourne (03) 9028 4862</a></h4>
		</div>
		<!-- circular navigation -->
		<div class="container home-container">
			<section class="menu menu--circle">
				<input type="checkbox" id="menu__active"/>
				<label for="menu__active" class="menu__active">
					<div class="menu__toggle">
						<div class="icon-nav">
							<div class="hamburger-nav"></div>
						</div>
					</div>
					<input name="arrow--up" type="radio" id="degree--up-0" value="hidden"/>
					<input type="radio" name="arrow--up" id="degree--up-1" />
					<input type="radio" name="arrow--up" id="degree--up-2" />

					<?php
					$ford_walker = new Ford_Walker;
					wp_nav_menu( array(
						'container_class' => 'menu__listings',
						'menu_class'      => 'circle',
						'theme_location'  => 'menu-1',
						'walker'          => $ford_walker,
					) );
					?>

					<div class="menu__arrow menu__arrow--top">
						<ul>
							<li>
								<label for="degree--up-0">
									<div class="arrow"></div>
								</label>
								<label for="degree--up-1">
									<div class="arrow"></div>
								</label>
								<label for="degree--up-2">
									<div class="arrow"></div>
								</label>
							</li>
						</ul>
					</div>
				</label>
			</section>
		</div>
