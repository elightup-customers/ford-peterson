<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ford
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-bottom">
		<div class="entry-content">
			<header class="entry-header">
				<?php
				if ( is_singular() ) :
					the_title( '<h1 class="entry-title">', '</h1>' );
				else :
					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				endif; ?>
			</header><!-- .entry-header -->
			<p>
			<?php echo wp_trim_words( get_the_content(), 40, '...' ); ?>
			</p>
			<footer class="entry-footer">
				<p class="link-more">
					<a href="<?php the_permalink(); ?>" class="tag-alike-style  more-link"><?php echo esc_html__( 'Read More', 'ford' ); ?></a>
				</p>
			</footer><!-- .entry-footer -->
		</div><!-- .entry-content -->
		<?php ford_post_thumbnail(); ?>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
