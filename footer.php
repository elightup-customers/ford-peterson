<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ford
 */

?>

	</div><!-- #content -->

	<?php
	// Check category_page or blog page.
	if ( is_404() || is_single() || is_category() ) {
		$style = '';
	} else {
		$page_id = get_the_ID();
		if ( ( is_home() && ! is_front_page() ) ) {
			$page_id = get_option( 'page_for_posts' );
		}
		$abstracts = rwmb_meta( 'image_abstract', ['size' => 'full'], $page_id );
		$abstract  = reset( $abstracts );
		$style     = $abstract ? 'style="background-image: url( ' . $abstract['url'] . ' )"' : '';
	}
	?>
	<div class="background-image" <?php echo wp_kses_post( $style ); ?>>
		<div class="site-content">
		</div>
	</div>

	<!-- Footer -->
	<div class="container footer">
		<div class="bluelines">
			<div class="btnbar"><img class="footbtn" src="<?php echo get_template_directory_uri(); ?>/images/footer_btn.png" alt="footer button">
				<button id="myBtn"><i class="fa fa-chevron-up"></i></button>
			</div>
		</div>
	</div>

	<!-- The Modal -->
	<div id="myModal" class="modal">
	  <!-- Modal content -->
		<div class="modal-content">
			<div class="addtxt">
				<div class="addtxt--left">
					<div class="socialbtn">
						<a href="https://www.linkedin.com/company/ford-peterson" target="_blank"><i class="fa fa-linkedin-square"></i></a>
						<a href="https://twitter.com/Ford_Peterson" target="_blank"><i class="fa fa-twitter-square"></i></a>
					</div>
					<div class="address">
						<div class="addtxt1">
							<h5>Sydney Office</h5>
							<p> Level 2<br>
							  309 George St<br>
							  Sydney<br>
							  NSW, 2000</p>
							<h5><a href="tel:0280073632">(02) 8007 3632</a></h5>
						</div>
						<div class="addtxt2">
							<h5>Melbourne Office</h5>
							<p>Level 13<br>
							  114 William St<br>
							  Melbourne<br>
							  VIC, 3000</p>
							<h5><a href="tel:0390284862">(03) 9028 4862</a></h5>
						</div>
						<div class="addtxt3">
							<img class="calogo" src="<?php echo get_template_directory_uri(); ?>/images/CA-footer.png" alt="Certified Acountants Logo" />
						</div>
					</div>
				</div>
				<div class="addtxt--right">
					<div class="entry-form">
						<?php
						$form_register = '[mb_user_profile_register id="form" label_submit="Register" confirmation="Your account has been created successfully!" password_strength=false]';
						echo '<h3>' . esc_html__( 'Stay informed', 'ford' ) . '</h3>';
						echo do_shortcode( $form_register );
						?>
					</div>
				</div>
			</div>
			<div class="close-btn">
				<span class="close">&times;
					<p>close</p>
				</span>
			</div>
			<div class="copy-right">
					©FPHQ Peterson. Site by <a href="https://18hands.com.au">18HANDS</a>
				</div>
		</div>
	</div>
	<!-- End Modal -->

</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
