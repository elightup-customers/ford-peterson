<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package ford
 */

get_header();
?>

	<main id="primary" class="site-main">
		<p class="image-page__logo">
			<img src="<?php echo get_template_directory_uri() ?>/images/recent_icon_sm.png">
		</p>
		<section class="content-area error-404 not-found">
			<header class="page-header">
				<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'ford' ); ?></h1>
				<img src="<?php echo get_template_directory_uri() ?>/images/heading_element.png">
			</header><!-- .page-header -->

			<div class="page-content">
				<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'ford' ); ?></p>

					<?php
					get_search_form();

					?>
			</div><!-- .page-content -->
			<?php ford_entry_form(); ?>
		</section><!-- .error-404 -->

	</main><!-- #main -->

<?php
get_sidebar();
get_footer();
