<?php
/**
 * The front page template file.
 *
 * If the user has selected a static page for their homepage, this is what will appear.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package ford
 */

// If users select to display blog posts on the front page, load the index file.
if ( 'posts' === get_option( 'show_on_front' ) ) {
	get_template_part( 'index' );
	return;
}

// Custom front page template.
get_header( 'home' );

?>

<!-- content -->
<div class="container bodypanel home-container">
	<div class="pnlhome1"> <img class="welcomeicon" src="<?php echo get_template_directory_uri(); ?>/images/welcome_icon_homepage.png" alt="welcome icon" />
		<div class="textpnlleft"> <img class="headerelement" src="<?php echo get_template_directory_uri(); ?>/images/heading_element.png" alt="heading icon" />
		  <p>Established in 2014 Ford Peterson is Australia’s leading accounting, finance and risk recruitment business.<br>
		    <br>
		    We actively work with candidates and clients in the following disciplines: <br>
		    <br>
		    <strong>
		    •	Accounting Professional Services<br>
		    •	Accounting Commerce<br>
		    •	Business Office Support<br>
			</strong>
		  </p>
		  <a href="<?php echo home_url(); ?>/about" target="_blank" class="button">find out more</a> <br>

		</div>
	</div>
	<div class="pnlhome2 ford-sidebar">
		<div class="logo2">
			<img class="blogicon" src="<?php echo get_template_directory_uri(); ?>/images/blog_icon_homepage.png" alt="blog icon" />
		</div>
		<div class="widget-content ford-sidebar__text">
			<?php
			$query = new WP_Query( array(
				'post_type'      => 'post',
				'posts_per_page' => 2,
			) );
			while ( $query->have_posts() ) : $query->the_post(); ?>
				<article class="aside-post ford-sidebar__item">
					<div class="ford-sidebar__content">
						<h4 class="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
						<?php the_excerpt(); ?>
						<div class="ford-sidebar__meta">
							<span class="posted-on">
								<?php echo esc_html( get_the_date() ); ?>
							</span>
						</div>
					</div>
					<div class="ford-sidebar__img">
						<a class="image" href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
					</div>
				</article>
			<?php endwhile; ?>
			<div class="ford-sidebar__readmore">
				<a href="<?php echo home_url(); ?>/blog" target="_blank"><?php echo esc_html__( 'Read more', 'ford' ); ?></a>
			</div>
		</div>
	</div>
</div>

<?php
get_footer( 'home' );
