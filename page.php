<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ford
 */

get_header();
?>

	<main id="primary" class="site-main">
		<p class="image-page__logo">
			<?php
			if ( has_post_thumbnail() ) {
				the_post_thumbnail();
			} else {
				?>
				<img src="<?php echo get_template_directory_uri() ?>/images/recent_icon_sm.png">
				<?php
			}
			?>
		</p>
		<div class="content-area">
			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
		</div>
	</main><!-- #main -->

<?php
get_sidebar();
get_footer();
